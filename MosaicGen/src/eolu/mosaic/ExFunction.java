package eolu.mosaic;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * This is a Function which handles exceptions. It's designed to be used with
 * flatMap, and will return an empty stream if an exception is thrown. For debug
 * use: Exceptions thrown are added to a static list.
 * 
 * TODO: Use CompletableFuture to wrap the result, as this has methods for more
 * proper handling of exceptions. You would get a stream containing a failed
 * future rather than an empty stream, and wouldn't have to deal with this
 * unfortunate static exception list.
 * 
 * @param <T> The parameter type.
 * @param <R> The return type.
 */
@FunctionalInterface
public interface ExFunction<T, R> extends Function<T, Stream<R>> {
    
    public static final List<String> EXCEPTIONS_THROWN = new ArrayList<>();
    
    /**
     * Functional call.
     * 
     * @param t The function param T.
     * @return R
     * @throws Exception
     */
    R applyEx(T t) throws Exception;
    
    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    @Override
    default Stream<R> apply(T t) {
        try {
            return Stream.of(applyEx(t));
        } catch (Exception e) {
            EXCEPTIONS_THROWN.add("[" + t.toString() + ":" + e.getMessage() + "]");
            return Stream.empty();
        }
    }
}