package eolu.mosaic;

import java.io.IOException;
import java.nio.file.Path;

public class MosaicGen {
    
    private static final Path SOURCE_IMAGE  = Path.of("C:/Users/TestUser/Pictures/IMG_0247.jpg");
    private static final Path CANDIDATE_DIR = Path.of("C:/Users/TestUser/Pictures/Candidates");
    private static final Path OUTPUT_IMAGE  = Path.of("C:/Users/TestUser/Pictures/Output.png");
    
    /**
     * Entry-point
     */
    public static void main(String... args) {
        try {
            // System.out.println("Supported formats: " +
            // Arrays.toString(ImageIO.getWriterFormatNames()));
            System.out.println("Loading source...");
            SourceImage src = new SourceImage(SOURCE_IMAGE.toUri().toURL());
            OutputImage out = new OutputImage(src, 1);
            out.createMosaic(new Candidates(CANDIDATE_DIR));
            out.writeToFile(OUTPUT_IMAGE);
            System.out.println("Threw " + ExFunction.EXCEPTIONS_THROWN.size() + " exceptions.");
            ExFunction.EXCEPTIONS_THROWN.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
