/*
 * Copyright 2019 Griffin O'Neill
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eolu.mosaic;

import static java.util.stream.IntStream.range;

import java.io.IOException;
import java.net.URL;

import javafx.scene.image.Image;

/**
 * This is the source image.
 */
public class SourceImage {
    
    // Loaded image data.
    private final Image image;
    private final int[] pixels;
    private final int   averageColor;
    private final int   height;
    private final int   width;
    
    /**
     * Constructor.
     * 
     * @throws IOException
     */
    public SourceImage(URL url) throws IOException, NullPointerException {
        image = new Image(url.openStream());
        height = (int) image.getHeight();
        width = (int) image.getWidth();
        pixels = new int[height * width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = x + width * y;
                pixels[index] = image.getPixelReader().getArgb(x, y);
            }
        }
        averageColor = calculateAverageColor();
    }
    
    /**
     * Get the average color of this image.
     */
    private int calculateAverageColor() {
        final int numPixels = getHeight() * getWidth();
        final int r = (int) Math.sqrt(range(0, numPixels).map(i -> getRpixel(i) * getRpixel(i)).average().orElse(0));
        final int g = (int) Math.sqrt(range(0, numPixels).map(i -> getGpixel(i) * getGpixel(i)).average().orElse(0));
        final int b = (int) Math.sqrt(range(0, numPixels).map(i -> getBpixel(i) * getBpixel(i)).average().orElse(0));
        return (r << 16) | (g << 8) | b;
    }
    
    public int getAverageColor() {
        return averageColor;
    }
    
    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }
    
    public int getPixel(int index) {
        return pixels[index];
    }
    
    public int getRpixel(int index) {
        return (pixels[index] >> 16) & 0xff;
    }
    
    public int getGpixel(int index) {
        return (pixels[index] >> 8) & 0xff;
    }
    
    public int getBpixel(int index) {
        return (pixels[index]) & 0xff;
    }
}
