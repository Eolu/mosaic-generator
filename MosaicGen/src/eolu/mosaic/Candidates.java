package eolu.mosaic;

import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

public class Candidates extends ArrayList<SourceImage> {
    private static final long         serialVersionUID = -9011882971256617344L;
    private static final List<String> IMAGE_TYPES      = List.of(ImageIO.getWriterFormatNames());
    
    public Candidates(Path rootDir) {
        System.out.println("Loading candidates...");
        getFilesRecursive(rootDir).filter(path -> IMAGE_TYPES.stream()
                                                             .filter(path.getFileName().toString()::endsWith)
                                                             .findAny()
                                                             .isPresent())
                                  .map(Path::toUri)
                                  .flatMap((ExFunction<URI, URL>) URI::toURL)
                                  .flatMap((ExFunction<URL, SourceImage>) SourceImage::new)
                                  .forEach(this::add);
    }
    
    /**
     * Get all files from a directory recursively.
     */
    public static Stream<Path> getFilesRecursive(Path rootDir) {
        return rootDir.toFile().exists() ?
                                         Stream.of(rootDir.toFile().list())
                                               .map(fileName -> Path.of(rootDir + "/" + fileName))
                                               .flatMap(path -> path.toFile().isDirectory() ? getFilesRecursive(path) : Stream.of(path)) :
                                         Stream.empty();
    }
}
