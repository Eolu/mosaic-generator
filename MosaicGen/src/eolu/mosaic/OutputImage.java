/*
 * Copyright 2019 Griffin O'Neill
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eolu.mosaic;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;

/**
 * This is the final result container.
 */
public class OutputImage {
    private static final int OUTPUT_SIZE = Integer.MAX_VALUE;
    
    private final BufferedImage image;
    private final int[][]       rgbValues;
    
    private final int sourceHeight;
    private final int sourceWidth;
    
    private final int mosaicSize;
    private final int clusterSize;
    
    public OutputImage(SourceImage source, int clusterSize) {
        this.clusterSize = clusterSize;
        sourceHeight = source.getHeight();
        sourceWidth = source.getWidth();
        
        // TODO: Customize pixel cluster size (so 1x1, 2x2, 3x3, etc). That way we can
        // have larger mosaics.
        mosaicSize = (int) (clusterSize * Math.sqrt(OUTPUT_SIZE / (sourceHeight * sourceWidth)));
        System.out.println("Mosaic tile size: " + mosaicSize);
        System.out.println("Mosaic width: " + sourceWidth * mosaicSize);
        System.out.println("Mosaic height: " + sourceHeight * mosaicSize);
        image = new BufferedImage((sourceWidth / clusterSize) * mosaicSize,
                                  (sourceHeight / clusterSize) * mosaicSize,
                                  BufferedImage.TYPE_INT_RGB);
        rgbValues = loadRgb(source);
    }
    
    public void createMosaic(List<SourceImage> candidates) {
        System.out.println("Creating mosaic...");
        for (int sy = 0; sy < sourceHeight / clusterSize; sy++) {
            for (int sx = 0; sx < sourceWidth / clusterSize; sx++) {
                
                // Init variables to find candidate
                double minDistance = Double.MAX_VALUE;
                int winnerIndex = -1;
                
                // Get source RGB values
                int[] rs = new int[clusterSize * clusterSize];
                int[] gs = new int[clusterSize * clusterSize];
                int[] bs = new int[clusterSize * clusterSize];
                for (int x = 0; x < clusterSize; x++) {
                    for (int y = 0; y < clusterSize; y++) {
                        int cIndex = x + clusterSize * y;
                        rs[cIndex] = (rgbValues[sx + x][sy + y] >> 16) & 0xFF;
                        gs[cIndex] = (rgbValues[sx + x][sy + y] >> 8) & 0xFF;
                        bs[cIndex] = rgbValues[sx + x][sy + y] & 0xFF;
                    }
                }
                final int r = IntStream.of(rs).map(i -> i * i).average().stream().mapToInt(d -> (int) Math.sqrt(d)).findAny().orElse(0);
                final int g = IntStream.of(gs).map(i -> i * i).average().stream().mapToInt(d -> (int) Math.sqrt(d)).findAny().orElse(0);
                final int b = IntStream.of(bs).map(i -> i * i).average().stream().mapToInt(d -> (int) Math.sqrt(d)).findAny().orElse(0);
                
                // Find most similar candidate
                for (int i = 0; i < candidates.size(); i++) {
                    // Get candidate RGB values
                    SourceImage cand = candidates.get(i);
                    int candR = (cand.getAverageColor() >> 16) & 0xFF;
                    int candG = (cand.getAverageColor() >> 8) & 0xFF;
                    int candB = (cand.getAverageColor()) & 0xFF;
                    
                    // Check distance between colors
                    double distance = Math.sqrt((r - candR) * (r - candR) + (g - candG) * (g - candG) + (b - candB) * (b - candB));
                    if (distance < minDistance) {
                        minDistance = distance;
                        winnerIndex = i;
                    }
                }
                
                // Found the closest image, use it
                SourceImage closest = candidates.get(winnerIndex);
                int height = closest.getHeight();
                int width = closest.getWidth();
                int startY = 0;
                int startX = 0;
                
                // TODO: Scale
                
                // Pick a random location in the image
                if (height > mosaicSize) {
                    startY = (int) (Math.random() * (height - mosaicSize));
                    // startY = (int) ((height / 2d) - (mosaicSize / 2d));
                }
                if (width > mosaicSize) {
                    startX = (int) (Math.random() * (width - mosaicSize));
                    // startX = (int) ((width / 2d) - (mosaicSize / 2d));
                }
                
                for (int y = startY; y < height && y < mosaicSize + startY; y++) {
                    for (int x = startX; x < width && x < mosaicSize + startX; x++) {
                        int index = x + width * y;
                        image.setRGB(sx * mosaicSize + (x - startX), sy * mosaicSize + (y - startY), closest.getPixel(index));
                    }
                }
                
            }
        }
    }
    
    /**
     * @brief Load RGB values from the given source image.
     * @param source The source image.
     * @return RGB values for X,Y coordinates in an array.
     */
    private int[][] loadRgb(SourceImage source) {
        int[][] rgbValues = new int[image.getWidth()][image.getHeight()];
        
        for (int y = 0; y < sourceHeight; y++) {
            for (int x = 0; x < sourceWidth; x++) {
                int index = x + sourceWidth * y;
                rgbValues[x][y] = source.getPixel(index);
            }
        }
        
        return rgbValues;
    }
    
    public void writeToFile(Path filePath) throws IOException {
        System.out.println("Writing to file " + filePath);
        ImageIO.write(image, filePath.toString().substring(filePath.toString().lastIndexOf('.') + 1), filePath.toFile());
    }
    
    public BufferedImage getImage() {
        return image;
    }
    
    public int getWidth() {
        return image.getWidth();
    }
    
    public int getHeight() {
        return image.getHeight();
    }
}
